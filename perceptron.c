#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<stdbool.h>

#define EPSILON 0.00001
#define MALE -1
#define FEMALE 1

typedef struct _Pattern
{
	double height;
	double weight;
	int sex; //-1 is male, 1 is female
} Pattern;

typedef struct _Perceptron
{
	double x;
	double y;
	double bias;
} Perceptron;

int activation_hard(Perceptron *perceptron, Pattern pattern)
{
	int val;
	if(((perceptron->x*pattern.height) + (perceptron->y*pattern.weight) + perceptron->bias) >= 0)
	{
		val = 1;
	}
	else
	{
		val = -1;
	}
	return val;
}

double activation_soft(Perceptron *perceptron, Pattern pattern)
{
	double val;
	val = atan((perceptron->x*pattern.height) + (perceptron->y*pattern.weight) + perceptron->bias);
	val *= 2;
	val /= M_PI;
	if(val < -0.9999)val = -1;
	else if(val > 0.9999)val = 1;
	return val;
}

void train(Perceptron *perceptron, Pattern *data, int numPoints, double percent, double alpha, bool hardActivation)
{
	int i = 0;
	int j = 0;
	int needed = floor(numPoints * percent);
	int trainSize = needed;
	bool *used = (bool *)calloc(numPoints, sizeof(bool));
	Pattern *train = (Pattern *)malloc(sizeof(Pattern) * needed);
	while(needed > 0)
	{
		i = rand() % numPoints;
		if(used[i])continue;
		else{
			used[i] = true;
			train[needed - 1] = data[i];
			needed--;
		}
	}
	double error = 0;
	for(i = 0; i < 1000; i++)
	{
		for(j = 0; j < trainSize; j++)
		{
			if(hardActivation)
			{
				error = train[j].sex - activation_hard(perceptron, train[j]);
			}
			else
			{
				error = train[j].sex - activation_soft(perceptron, train[j]);
			}
			double delta = error*alpha;
			perceptron->x += delta* train[j].height;
			perceptron->y += delta* train[j].weight;
			perceptron->bias += delta;
		}
		error = 0;
		for(j = 0; j < trainSize; j++)
		{
			if(hardActivation)
			{
				error += pow(train[j].sex - activation_hard(perceptron, train[j]), 2);
			}
			else
			{
				error += pow(train[j].sex - activation_soft(perceptron, train[j]), 2);
			}
		}
		error /= trainSize;
		if(error < EPSILON)
		{
			printf("Error < EPSILON after iteration %i\n", i);
			goto train_done;
		}
	}
train_done:
	//Do testing here
	printf("Training done, printing results for %s, %f for training\n", hardActivation ? "hard activation" : "soft activation", percent);
	printf("final weights : \n x: %f \n y: %f \n bias: %f\n", perceptron->x, perceptron->y, perceptron->bias);
	double tm = 0, tf = 0, fm = 0, ff = 0, Tm = 0, Tf = 0; 	//t = true, f = false, T = Total
	for(i = 0; i < numPoints; i++)
	{
		if(!used[i])
		{
			//printf("checking %i\n", i);
			if(hardActivation)
			{
				int res = activation_hard(perceptron, data[i]);
				if(data[i].sex != activation_hard(perceptron, data[i]))
				{
					//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! res : %i\n", res);
					//printf("Misclassified entry %i, actual %i, predicted %i\n", i, data[i].sex, res);
					if(data[i].sex == MALE)
					{
						ff++;
						Tm++;
					}
					else 
					{
						fm++;
						Tf++;
					}
				}
				else
				{
					//printf("Correctly classified entry %i\n", i);
					if(data[i].sex == MALE)
					{
						tm++;
						Tm++;
					}
					else 
					{
						tf++;
						Tf++;
					}
				}
			}//end if hardActivation
			else
			{
				double res = activation_soft(perceptron, data[i]);
				if((data[i].sex > 0 && activation_soft(perceptron, data[i]) > 0) || (data[i].sex < 0 && activation_soft(perceptron, data[i]) < 0))
				{
					//printf("Correctly classified entry %i, actual %i, predicted %f\n", i, data[i].sex, res);
					if(data[i].sex == MALE)
					{
						tm++;
						Tm++;
					}
					else 
					{
						tf++;
						Tf++;
					}
				}
				else
				{
					//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! res : %f\n", res);
					//printf("Misclassified entry %i, actual %i, predicted %f\n", i, data[i].sex, res);
					if(data[i].sex == MALE)
					{
						ff++;
						Tm++;
					}
					else 
					{
						fm++;
						Tf++;
					}
				}
			}//end else
		}//end if !used[i]
	}//end for i ... numPoints
	printf("Error rates : \n");
	printf("True Positive rate : %f\n", ((tm/Tm)*100));
	printf("True Negative rate : %f\n", ((tf/Tf)*100));
	printf("False positive rate : %f\n", ((fm/Tm)*100));
	printf("False negative rate : %f\n", ((ff/Tf)*100));
}//end train

int main(void)
{
	FILE *input;
	Perceptron perceptron;
	Perceptron h25, h50, h75, s25, s50, s75; 	//h : hard activation, s: soft activation, 25 - 25 % train, 75 - 75% train
	Pattern data[4000];
	double alpha = .1;
	char str[256];
	input = fopen("data.csv", "r");
	int i = 0;
	double h, w;
	int s;
	char *sh, *sw, *ss;
	while(fgets(str, 100, input))
	{
		sscanf(str, "%lf,%lf,%i,\n", &h, &w, &s);
		data[i].height = h;
		data[i].weight = w;
		data[i].sex = (s == 0) ? -1 : s; //Sets sex to -1 for male, 1 for female
		i++;
	}

	perceptron.x = 1;
	perceptron.y = 35;
	perceptron.bias = -2490;
	h25 = h75 = s25 = s75 = h50 = s50 = perceptron;
	train(&h25, data, 4000, 0.25, alpha, true);
	//train(&h50, data, 4000, 0.50, alpha, true);
	train(&h75, data, 4000, 0.75, alpha, true);
	train(&s25, data, 4000, 0.25, alpha, false);
	//train(&s50, data, 4000, 0.50, alpha, false);
	train(&s75, data, 4000, 0.75, alpha, false);
	return 0;
}
